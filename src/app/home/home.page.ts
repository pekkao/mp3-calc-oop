import { Component } from '@angular/core';
import { CalculatorService } from '../calculator.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {

  // Member variables
  private totalSpace: number = 0;
  private usedSpace: number = 0;
  private nbrOfTracks: number = 0;

  constructor(private calculatorService: CalculatorService) {}

  private calculate() {
    this.calculatorService.setInputs(
      this.totalSpace,
      this.usedSpace,
      this.nbrOfTracks);
  }

}

