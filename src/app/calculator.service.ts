import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class CalculatorService {

  // Member variables
  private totalSpace: number = 0;
  private usedSpace: number = 0;
  private nbrOfTracks: number = 0;
  private remainingSpace: number = 0;
  private averageTrackSpace: number = 0;
  private nbrOfTracksLeft: number = 0;

  constructor() { }

  public setInputs(
    totalSpace: number, 
    usedSpace: number, 
    nbrOfTracks: number) {
    this.totalSpace = totalSpace;
    this.usedSpace = usedSpace;
    this.nbrOfTracks = nbrOfTracks;
  }

  public getRemainingSpace():number {
    this.remainingSpace = this.totalSpace - this.usedSpace;
    return this.remainingSpace;
  }

  public getAverageTrackSpace():number {
    this.averageTrackSpace = this.usedSpace / this.nbrOfTracks;
    return isNaN(this.averageTrackSpace) ? 0 : this.averageTrackSpace;
  }

  public getNbrOfTracksLeft():number {
    this.nbrOfTracksLeft = 
      Math.floor(this.remainingSpace / this.averageTrackSpace);
    return isNaN(this.nbrOfTracksLeft) ? 0 : this.nbrOfTracksLeft;
  }

}